package shift.reduce;

import java.util.LinkedList;
import java.util.List;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author bruno
 */
public class Processing {

    private List<String> leftStack = new LinkedList<>();
    private List<String> rightStack = new LinkedList<>();
    private JTable tableStackGUI = null;
    private int tableCountLines = 0;

    public Processing() {
    }

    public void setTableStackGUI(JTable TableStackGUI) {
        this.tableStackGUI = TableStackGUI;
    }

    public Processing(List<String> Stack_R) {
        this.rightStack = Stack_R;
    }

    public void ShowStack(List<String> stack) {
        stack.forEach((value) -> {
            System.out.println(value);
        });
    }

    public List<String> getLeftStack() {
        return leftStack;
    }

    public List<String> getRightStack() {
        return rightStack;
    }
    
    private void refreshTableGUI() {
        if(tableStackGUI != null) {
            String lineL = "";
            String lineR = "";
            for (int i = 0; i < (leftStack.size() > rightStack.size() ? leftStack.size() : rightStack.size()); i++) {
                lineL += (i < leftStack.size() ? leftStack.get(i) + (leftStack.size() -1 != i ? " | " : "") : "");
                lineR += (i < rightStack.size() ? rightStack.get(i) + (rightStack.size() -1 != i ? " | " : "") : "");
            }
            DefaultTableModel model = (DefaultTableModel) tableStackGUI.getModel();
            model.addRow(new Object[]{ tableCountLines, lineL, lineR });
            tableCountLines++;
        }
    }

    public void addInRightStack(String value) {
        rightStack.add(value);
        refreshTableGUI();
    }

    public void transfer() {
        if (rightStack.size() > 0) {
            leftStack.add(rightStack.get(0));
            rightStack.remove(0);
            refreshTableGUI();
        }
    }
    
    public double getResult() {
        refreshTableGUI();
        if(leftStack.size() <= 0) return 0.0;
        return Double.parseDouble(leftStack.get(0));
    }
    
    public float opera(int index, String operation) {
        float value = 0;
        if (index > leftStack.size() - 1) return 0;
        
        refreshTableGUI();
        
        final float n1 = Float.parseFloat(leftStack.get(index - 1)),
                    n2 = Float.parseFloat(leftStack.get(index));
        
        switch (operation) {
            case "^":
                value = (float) Math.pow(n1, n2);
                break;
            case "*":
                value = n1 * n2;
                break;
            case "/":
                value = n1 / n2;
                break;
            case "%":
                value = n1 % n2;
                break;
            case "+":
                value = n1 + n2;
                break;
            case "-":
                value = n1 - n2;
                break;
            default:
                return value;
        }
        leftStack.set(index -1, String.valueOf(value));
        leftStack.remove(index);
        return value;
    }
}
