package shift.reduce;

import java.util.Arrays;
import java.util.List;

/**
 *
 * @author bruno
 */
public class Lexer {
    private String font = "";
    private final Processing processing;
    private static final List<String> SPECIAL_SYMBOLS = Arrays.asList(
            "+", "-", "*", "/", "(", ")", "^", "%"
    );

    public Lexer(String font, Processing processing) {
        this.font = font;
        this.processing = processing;
    }

    public String isNumber() {
        String result = "";
        int i = 0, state = 0;
        while (true) {
            String expression = "$";
            
            if (i < font.length()) {
                expression = font.substring(i, i + 1);
            }
            
            final boolean isNumber = expression.charAt(0) >= '0' 
                                  && expression.charAt(0) <= '9';
            
            switch (state) {
                case 0:
                    if (isNumber) {
                        state = 1;
                        result += expression;
                        i++;
                    } else {
                        return "";
                    }
                    break;
                case 1:
                    if (isNumber) {
                        state = 1;
                        result += expression;
                        i++;
                    } else if (expression.equals(".")) {
                        state = 2;
                        result += expression;
                        i++;
                    } else {
                        state = 4;
                    }
                    break;
                case 2:
                    if (isNumber) {
                        state = 3;
                        result += expression;
                        i++;
                    } else {
                        return "";
                    }
                    break;
                case 3:
                    if (isNumber) {
                        state = 3;
                        result += expression;
                        i++;
                    } else {
                        state = 4;
                    }
                    break;
                case 4:
                    processing.addInRightStack(result);
                    font = font.substring(i);
                    return result;
                default:
                    break;
            }

            if (font.length() == 0) {
                return result;
            }
        }
    }

    public String isOperator() {
        String expression = font.substring(0, 1);
        for (String operator : SPECIAL_SYMBOLS) if (expression.equals(operator)) {
            font = font.substring(1);
            return expression;
        }
        return "";
    }

    public String analize() {
        String result = "";
        while (true) {
            String token = isNumber();
            if (token.length() > 0) {
                result += "i";
            } else {
                token = isOperator();
                if (token.length() > 0) {
                    result += token;
                }
            }

            if (token.length() == 0) {
                System.out.println("ERRO");
                return "";
            } else {
                if (font.length() == 0) {
                    return result;
                }
            }
        }
    }
}
