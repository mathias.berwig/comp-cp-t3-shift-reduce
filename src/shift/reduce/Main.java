package shift.reduce;

/**
 *
 * @author bruno
 */
public class Main {
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        /*
        // Teste 1
        ShiftReduce sr = new ShiftReduce();
        sr.stack_L = "";
        sr.stack_R = "(i+i)*i";
        //System.out.println("Inicio: " + sr.Eh_inicio_de_regra());
        //System.out.println("Reduce: " + sr.reduce("T*F"));
        System.out.println(sr.DoStuff());
         */

        String input = "12*3.2+15*3+(2+7)";
        Processing processing = new Processing();
        
        Lexer lexico = new Lexer(input, processing);
        String expressao = lexico.analize();
        if (expressao.length() > 0) {
            ShiftReduce sr = new ShiftReduce();
            sr.setLeftStack("");
            sr.setRightStack(expressao);
            if (sr.doStuff(processing)) {
                System.out.println("aceita!!!");
            } else {
                System.out.println("rejeita!!!");
            }
        }

        //processing.ShowStack(processing.getStack_L());
        //processing.ShowStack(processing.getStack_R());
        System.out.println(processing.getResult());
    }
    
    /*
        pilha de execução. A pilha de execução e igual a pilha de redução porem mexe com os valores.
        
        quando mexe com i tras da pilha de execução.
        quando redus dois simbolos pra 1 aplica-se a operação!
    
        metodo que pegue os indices correspondentes da pilha de processamento. ignorando os operadores para pegar o indice.
        realocar a pilha de processamento removendo espaços vazios produzidos pelas operações.
    */
}
