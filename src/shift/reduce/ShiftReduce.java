package shift.reduce;

import java.util.Arrays;
import java.util.List;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author bruno
 */
public class ShiftReduce {
    /* 
        LINGUAGEM
    
        E => E+T|T
        T => T*F|F
        F => (E)|i
    
        'i' seria a variavel da linguagem.
    
        Gramatica simples!
        { "A", "A/F", "F" },
        { "F", "(A)", "i" },
     */
    private String[][] grammar;
    private String leftStack = "", 
                   rightStack = "";
    private JTable tableStackGUI = null;
    private int tableCountLines = 0;
    private static final List<Character> OPERATORS = Arrays.asList(
            '+', '-', '*', '/', '(', ')', '^', '%'
    );

    public void setGrammar(String[][] grammar) {
        this.grammar = grammar;
    }

    public void setLeftStack(String leftStack) {
        this.leftStack = leftStack;
    }

    public void setRightStack(String rightStack) {
        this.rightStack = rightStack;
    }

    public void setTableStackGUI(JTable TableStackGUI) {
        this.tableStackGUI = TableStackGUI;
    }

    public void transfer() {
        if (rightStack.length() > 0) {
            leftStack = leftStack + rightStack.substring(0, 1);
            rightStack = rightStack.substring(1);
        }
    }

    public int isInitOfRule() {
        if (rightStack.length() == 0) return -1;

        int count = 0;
        while (count < leftStack.length()) {
            final String expression = leftStack.substring(count) 
                                    + rightStack.substring(0, 1);

            for (String[] rule : grammar) {
                for (int i = 1; i < rule.length; i++) {

                    if (rule[i].length() >= expression.length()
                            && expression.equals(rule[i].substring(0, expression.length()))) {
                        return count;
                    }
                }
            }
            count++;
        }
        return -1;
    }

    public String reduce(String expression) {
        for (String[] rule : grammar) {
            for (int i = 1; i < rule.length; i++) {
                // Retorna o início da regra
                if (expression.equals(rule[i])) return rule[0];
            }
        }
        return "";
    }

    public boolean tryReduce() {
        int count = 0;
        while (count < leftStack.length()) {
            final String reduced = reduce(leftStack.substring(count));
            if (!reduced.equals("")) {
                leftStack = leftStack.substring(0, count) + reduced;
                return true;
            }
            count++;
        }
        return false;
    }

    public String getLastStackTerminalOperator(String lastStack) {
        for (int i = lastStack.length() -1; i > 0; i--) {
            final int indexOf = OPERATORS.indexOf(lastStack.charAt(i));
            if (indexOf > -1) {
                return OPERATORS.get(indexOf).toString();
            }
        }
        return "";
    }
    
    public boolean doStuff(Processing processing) {
        int auxLastStackReduceCount = 0;
        String auxLastLeftStack = "";

        while (true) {
            refreshTableGUI(leftStack, rightStack);

            if (leftStack.length() > 0) {
                if (leftStack.substring(leftStack.length() - 1).equals("i")) {
                    processing.transfer();
                } else {
                    if (leftStack.replaceAll("[(]|[)]", "").length() < auxLastStackReduceCount) {
                        processing.opera(
                                auxLastLeftStack.replaceAll(
                                        "[+]|[-]|[*]|[\\/]|[(]|[)]|[\\^]|[%]", ""
                                ).length() - 1,
                                getLastStackTerminalOperator(auxLastLeftStack)
                        );
                    }
                }
            }
            auxLastStackReduceCount = leftStack.replaceAll("[(]|[)]", "").length();
            auxLastLeftStack = leftStack;

            boolean reduced = false;
            final int auxRightStackLength = rightStack.length();
            if (isInitOfRule() >= 0) {
                transfer();
            } else {
                reduced = tryReduce();
                if (!reduced) transfer();
            }

            if (auxRightStackLength == 0 && reduced == false) {
                return leftStack.equals(grammar[0][0]);
            }
        }
    }
    
    private void refreshTableGUI(String expressionL, String expressionR) {
        if (tableStackGUI != null) {
            DefaultTableModel model = (DefaultTableModel) tableStackGUI.getModel();
            model.addRow(new Object[]{
                tableCountLines, expressionL, expressionR 
            });
            tableCountLines++;
        }
    }
}